{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Norms\n",
    "A norm is a \"measure\" of a vector, often this can be interpreted as a length. In particular the L2 norm:\n",
    "\n",
    "$$ \\|\\mathbf{x}\\|_2 \\quad\\!\\!:=\\quad\\!\\! \\sqrt{\\mathbf{x}^T \\mathbf{x}} \\quad\\!\\!=\\quad\\!\\! \\sqrt{\\sum_{i=1}^d x_i^2}$$\n",
    "\n",
    "is the natural measure of length in Euclidean space, and is the generalisation of the pythagorean formula $\\sqrt{x_1^2 + x_2^2}$. We have previously discussed that the overall size of the residuals $\\mathbf{y} - \\hat{\\mathbf{y}}$ of a model prediction can be measured via their norm, e.g.\n",
    "\n",
    "$$ r \\quad\\!\\! = \\quad\\!\\! \\|\\mathbf{y} - \\hat{\\mathbf{y}}\\|_2$$\n",
    "\n",
    "and naturally, if our goal is to minimise error, it is equivalent to minimising such residuals. The choice of norm for which to measure residuals is important, but we will not discuss it much. For an overview:\n",
    "\n",
    "* $L_2$: $\\left(\\sqrt{\\sum_{i=1}^d x_i^2}\\right)$. The **default** choice. Used primarily for mathematical tractability; it is the easiest norm to optimize. It is quite magnitude averse, preferring lots of medium errors rather than some small and some large.\n",
    "* $L_1$: $\\left(\\sum_{i=1}^d |x_i|\\right)$. Use for **more robust** analyses. Notice that no element is squared, and extreme values are not penalised so heavily. \n",
    "* Other norms ($L_0, L_{\\infty}$ or $L_p$ for general $p$). **Not often useful in practice**. These norms occasionally come in useful, but tend to be used in a relatively specialist way. These may be of interest for further study, but we will not discuss them further."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Monotone Transformation\n",
    "Optimizing a function $f(x)$ is equivalent to optimizing $g(f(x))$ for any *monotone* function $g$. Examples of monotone functions include:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f, axs = plt.subplots(1,5, figsize=(12,3))\n",
    "def pltfn(f, a, b, title, ax): \n",
    "    ax.plot(np.linspace(a, b, 100), f(np.linspace(a, b, 100)))\n",
    "    ax.set_title(title);\n",
    "\n",
    "pltfn(lambda x: 2*x, -10, 10, \"g(x) = 2x\", axs[0])\n",
    "pltfn(np.exp, -10, 10, \"g(x) = exp(x)\", axs[1])\n",
    "pltfn(np.log, 1e-4, 10, \"g(x) = log(x)\", axs[2])\n",
    "pltfn(np.sqrt, 1e-4, 10, \"g(x) = $\\sqrt{x}$\", axs[3])\n",
    "pltfn(lambda x: x**2, 1e-4, 10, \"$g(x) = x^2$\", axs[4])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These functions are all monotone increasing, meaning that if $x_2 > x_1$ then $g(x_2) > g(x_1)$. Notice that the domain of the 3rd/4th functions, log(x) and $\\sqrt{x}$, are non-negative (strictly positive in the case of log), and are undefined for negative inputs. Keen readers may have noticed that the final example $g(x) = x^2$ is not monotone in general, but is monotone if we restrict it to a non-negative range $[0, +\\infty)$.\n",
    "\n",
    "The effect of a monotone transformation on another function is to squash and stretch the y-axis of the original function. Notice in the following examples, the position of the maximum of the function remains the same."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f, axs = plt.subplots(4,2, figsize=(10,7))\n",
    "pltfn(lambda x: np.exp(-x**2), -3, 3, \"f(x)\", axs[0,0])\n",
    "pltfn(lambda x: 2*np.exp(-x**2), -3, 3, \"2*(f(x))\", axs[0,1])\n",
    "pltfn(lambda x: np.exp(-x**2), -3, 3, \"f(x)\", axs[1,0])\n",
    "pltfn(lambda x: -x**2, -3, 3, \"log(f(x))\", axs[1,1])\n",
    "pltfn(lambda x: np.exp(np.sin(x)/((x)**2+ 1e-1)), -5, 5, \"f(x)\", axs[2,0])\n",
    "pltfn(lambda x: np.exp(np.sin(x)/((x)**2+ 1e-1))**0.5, -5, 5, \"sqrt(f(x))\", axs[2,1])\n",
    "pltfn(lambda x: np.exp(abs(np.sin(x))/(x**2+ 1e-5)), -5, 5, \"f(x)\", axs[3,0])\n",
    "pltfn(lambda x: np.sqrt(abs(np.sin(x))/(x**2+ 1e-5)), -5, 5, \"sqrt(log(f(x)))\", axs[3,1])\n",
    "plt.tight_layout()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style=\"color:red\"><b>[Question]:</b></span> Which of these functions do you think are hardest to optimise / find the maximum value?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Questions:\n",
    "* Note that the linear regression loss $\\|y - Xw\\|_2^2$ is also equivalent by monotone transform to:\n",
    "    * $\\arg\\min_w \\|y - Xw\\|_2$\n",
    "    * $\\arg\\max_w \\exp\\{-\\frac{1}{2}\\|y - Xw\\|_2\\}$\n",
    "    \n",
    " Convince yourself this is true. Which of these three expressions is easiest to optimise?\n",
    "<br>\n",
    "<br>\n",
    "<br>\n",
    "* Why property does the transform $g(x) = \\log(x)$ have which might be especially useful for various problems?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calculating the derivative for Multivariate Linear Regression\n",
    "\n",
    "Our goal is to find:\n",
    "\n",
    "$$\n",
    "w^\\star \\quad\\!\\!=\\quad\\!\\! \\arg\\min_w\\|y-Xw\\|_2^2 \\quad\\!\\!=\\quad\\!\\! \\arg\\min_w\\,\\,(y-Xw)^T (y-Xw)\n",
    "$$\n",
    "Then,\n",
    "$$\n",
    "\\arg\\min_w\\,\\,(y-Xw)^T (y-Xw) \\quad\\!\\!=\\quad\\!\\! \\arg\\min_w\\,\\,-2w^T X^Ty + w^T X^T Xw\n",
    "$$\n",
    "\n",
    "This is a simple (multivariate) quadratic equation, whose derivative therefore is:\n",
    "\n",
    "$$ \\nabla_w \\|y - Xw\\|_2^2 \\quad\\!\\!=\\quad\\!\\! -2X^T y + 2X^T X w $$\n",
    "\n",
    "If you are more familiar with univariate calculus, imagine each of these variables is a scalar quantity, and note that in this case $X^T = X$. By commuting the terms, you may observe that this is the correct scalar derivative. Note: usually the objective is written after scaling by $\\frac{1}{2}$, but this is mathematical rhetoric <span style=\"color:red\"><b>[Question:</b></span> Why? <span style=\"color:red\"><b>]</b></span>.\n",
    "\n",
    "By setting the gradient to zero, you may obtain a simple closed form expression for the minimum value, without needing to resort to much slower gradient-based optimisation. <span style=\"color:red\"><b>[Question:</b></span> What is the solution? <span style=\"color:red\"><b>]</b></span>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
