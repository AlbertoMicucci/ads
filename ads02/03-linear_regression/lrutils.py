import numpy as np
import matplotlib.pyplot as plt

def generate_data(n):
    """
    generate data for the Linear Regression exercises. Ideally specify the
    random seed: 1000 before calling. In our examples we have used `n=200`.
    """
    x = (np.random.rand(n)-0.2)*160
    y = 1.8*np.log(x+36) + np.exp(x/10-10.5) + 0.02*x  + np.random.randn(n)*2
    x = x.reshape(n,1)
    return x, y


def generate_data3d(n):
    """
    generate 3D data for the Linear Regression exercises. Ideally specify the
    random seed: 3 before calling. In our examples we have used `n=200`.
    """
    x = (np.random.multivariate_normal(np.array([1,2]), np.array([[3, 2.6], [2.6, 3]]), size=n))
    w = np.random.randn(2)
    v = x @w
    y = np.exp(np.sqrt(np.abs(v-np.percentile(v, 0.9)))) + 0.08*v  + np.random.randn(n)*0.3
    return x, y


def plot_abline(slope, intercept, *args, **kwargs):
    """
    Plot a line from slope and intercept.
    If desired, an axis handle can be specified.
    """
    ax = plt.gca() if "ax" not in kwargs else kwargs.pop("ax")
    x_vals = np.array(ax.get_xlim())
    y_vals = intercept + slope * x_vals
    ax.plot(x_vals, y_vals, *args, **kwargs)