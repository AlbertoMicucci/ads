# Create environment
conda create -n ads04 python=3 jupyter numpy pandas matplotlib seaborn python-graphviz scikit-learn pandas-datareader xlrd

## Activate environment
source activate ads04
## or
conda activate ads04

## install prophet
conda install -c conda-forge fbprophet

# Troubleshooting

On Windows, FBProphet seems to be causing trouble with Python 3.7. If you have problems, create the environment with `python=3.5.2`.